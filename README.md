# Bioeng PY

Full Open Source Python Bayesian Inference

## Setup

```sh
# If using system python on ubuntu 20.04
sudo apt install python3 python3-venv

# If using Anaconda use an env with python 3.9
# and activate it before creating a new venv
conda create -n env_name python=3.9
conda activate env_name

# If you have python3.8 installed proceed from here
python3 -m venv .venv

# Activate venv
# For fish shell
# source .venv/bin/activate.fish
source .venv/bin/activate

pip install --upgrade setuptools wheel pip-tools
pip-sync requirements/requirements_dev.txt

# NOTE!: this may not work on Windows OS
```

# Running scripts

``` sh
# Activate venv if needed
source .venv/bin/activate

# We modify PYTHONPATH to include root of repo
# So we can keep scripts in subdirectories
# If you are currently in repo root folder you can do
export PYTHONPATH=$PWD

# Run scripts from root of repo such as
python __script_name__.py
```

# Notes

- The pipeline for the bayesian part is located in
    *bayesian/processing.sh*

- The bayesian algorithm is located at
    *bayesian/processing/bayesian_inference.py*
