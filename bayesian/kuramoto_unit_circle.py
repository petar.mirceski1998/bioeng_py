from typing import Tuple

import matplotlib.pyplot as plt
import numpy as np
import numpy.typing as npt
from processing.synchronization import (
    phase_synchronization_index,
    synchronization_angle,
)
from processing.utils.constants import KURAMOTO_INDEX_SUBJECT_PATH, PLOT_KURAMOTO_PATH
from processing.utils.data_utils import check_folder, load_phases
from tqdm import tqdm


def draw_circle() -> Tuple[
    npt.NDArray[np.float64], npt.NDArray[np.float64], npt.NDArray[np.float64]
]:
    x = np.linspace(-1.0, 1.0, 100)
    y = np.linspace(-1.0, 1.0, 100)

    X, Y = np.meshgrid(x, y)

    F = X ** 2 + Y ** 2 - 1.0
    return X, Y, F


def plot(phase_differences: npt.NDArray[np.float64]) -> None:

    X, Y, F = draw_circle()
    fig, ax = plt.subplots()

    ax.contour(X, Y, F, [0])

    for phase in phase_differences:
        x_coord = np.cos(phase)
        y_coord = np.sin(phase)
        ax.scatter(x_coord, y_coord, color="red")

    synchronization_index = phase_synchronization_index(phase_differences)

    angle = synchronization_angle(phase_differences)

    dx_arrow = synchronization_index * np.cos(angle)
    dy_arrow = synchronization_index * np.sin(angle)

    plt.arrow(
        x=0, y=0, dx=dx_arrow, dy=dy_arrow, head_width=0.05, length_includes_head=True
    )

    ax.set_aspect(1)

    plt.title("Unit circle")

    plt.xlim(-1.25, 1.25)
    plt.ylim(-1.25, 1.25)

    plt.grid(linestyle="--")


def kuramoto_main() -> None:
    check_folder(PLOT_KURAMOTO_PATH)
    for file_path in tqdm(KURAMOTO_INDEX_SUBJECT_PATH):
        first_phases, second_phases = load_phases(file_path)
        phase_differences = first_phases - second_phases
        # we only need some of the data points
        phase_differences = phase_differences[:, 0:150]
        index = phase_synchronization_index(phase_differences)

        plot(phase_differences)
        save_name = str(round(index, 2)).replace(".", "_")
        plt.savefig(PLOT_KURAMOTO_PATH / f"unit_circle_{save_name}.png")
        plt.close()


if __name__ == "__main__":
    kuramoto_main()
