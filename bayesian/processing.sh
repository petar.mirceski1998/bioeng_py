#!/usr/bin/env bash
# set -euo pipefail

pythonFiles=(
  "unpacking.py"
  "phase_from_signals.py"
  "eeg_plot.py"
  "kuramoto_unit_circle.py"
  "hilbert_plots.py"
  "bayesian_data_save.py"
  "bayesian_plot_save.py"
  "failed_histogram.py"
  "mean_freq_coupling_plots.py"
  "mean_state_coupling_plots.py"
  "net_coupling_maps_freq.py"
  "net_coupling_maps_state.py"
  "p_values_coupling_strength_channels.py"
  "p_values_coupling_strength_states.py"
  "p_values_coupling_strength_frequency.py"
)

counter=1
for file in "${pythonFiles[@]}"; do
  echo "file ${counter} of ${#pythonFiles[@]}"
  echo "$file"
  counter=$((counter + 1))
  python "$file"
done
