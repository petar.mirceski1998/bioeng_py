import matplotlib.pyplot as plt
import numpy as np
from processing.utils.constants import (
    CHANNELS,
    EEG_PATIENT_PATH,
    EEG_PLOT_PATH,
    RE_SAMPLE_FREQ,
)
from processing.utils.data_utils import check_folder, load_patient

TIME_INDEX: int = 500
PADDING_POINTS: int = 5


def plot_eeg_of_patient(patient: np.ndarray) -> None:
    TIME_INDEX = 500
    time = np.arange(TIME_INDEX) / RE_SAMPLE_FREQ
    fig, axes = plt.subplots(nrows=4, ncols=1, figsize=(16, 10))
    fig.suptitle("Five seconds of an EEG recording", fontsize=16)
    plt.setp(axes.flat, ylabel="Amplitude(mv)", xlabel="Time(ms)")

    for ax, row in zip(axes, CHANNELS):
        ax.annotate(
            row,
            xy=(0, 0.5),
            xytext=(-ax.yaxis.labelpad - PADDING_POINTS, 0),
            xycoords=ax.yaxis.label,
            textcoords="offset points",
            size="large",
            ha="right",
            va="center",
        )

    fig.tight_layout()

    fig.subplots_adjust(left=0.1, top=0.95)
    for index, ax in enumerate(axes):
        eeg = patient[index, :TIME_INDEX]
        ax.plot(time, eeg)
    plt.savefig(str(EEG_PLOT_PATH / "common_eeg.png"), dpi=140)
    plt.show()


def main_eeg_plot() -> None:
    check_folder(EEG_PLOT_PATH)
    patient = load_patient(EEG_PATIENT_PATH)
    plot_eeg_of_patient(patient)


if __name__ == "__main__":
    main_eeg_plot()
