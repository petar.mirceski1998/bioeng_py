import pickle
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import numpy.typing as npt
import scipy.signal
from processing.utils.constants import (
    HILBERT_FIG_PATH,
    HILBERT_SUBJECT_PATH,
    RE_SAMPLE_FREQ,
)
from processing.utils.data_utils import check_folder


def co_hilbproto(
    signal: npt.NDArray[np.complex64],
) -> npt.NDArray[np.complex64]:
    """INPUT:  x      is scalar timeseries,
            x0,y0  are coordinates of the origin (by default x0=0, y0=0)
            ntail  is the number of points at the ends to be cut off,
                   by default ntail=1000
    Output: theta is the protophase in 0,2pi interval
            minamp is the minimal instantaneous amplitude over the average
            instantaneous amplitude
    """
    hilbert_tf = scipy.signal.hilbert(signal)
    hilbert_tf = hilbert_tf - np.mean(hilbert_tf)
    return np.array(hilbert_tf).astype(np.complex64)


def load_signals(file_path: Path) -> npt.NDArray[np.float64]:
    infile = open(str(file_path), "rb")
    patient = pickle.load(infile, encoding="utf-8")
    infile.close()
    return np.array(patient["eeg"])


def hilbert_main() -> None:
    subject = load_signals(HILBERT_SUBJECT_PATH)
    eeg = subject[3, :300]
    time = np.arange(300) / RE_SAMPLE_FREQ
    hilbert = co_hilbproto(eeg)

    plt.figure(figsize=(16.0, 10.0))

    # REVIEW: This is bad imho try to minimize it
    ax1 = plt.subplot(221)
    ax1.set_title("Signal Amplitude")
    ax1.plot(time, eeg, label="eeg channel")
    ax1.plot(time, hilbert.imag, label="analytical signal")
    ax1.grid(True)
    ax1.set_xlabel("Time(s)")
    ax1.set_ylabel("Amplitude")
    ax1.legend()

    ax2 = plt.subplot(222)
    ax2.set_title("Polar Plot of Analytical Signal")
    ax2.plot(hilbert.real, hilbert.imag)
    ax2.set_ylabel(r"$\dot{\phi}$")
    ax2.set_xlabel(r"$\phi$")
    ax2.grid(True)

    ax3 = plt.subplot(223)
    ax3.set_title("Analytical Phase")
    ax3.plot(time, np.angle(hilbert) % (2 * np.pi), label="wrapped phase")
    ax3.plot(time, np.unwrap(np.angle(hilbert) % (2 * np.pi)), label="un-wrapped phase")
    ax3.set_xlabel("Time(s)")
    ax3.legend()
    ax3.grid(True)

    ax4 = plt.subplot(224)
    ax4.set_title("Hilbert Signal")
    ax4.plot(time, np.abs(hilbert))
    ax4.set_xlabel("Time(s)")
    ax4.grid(True)

    plt.tight_layout()
    plt.savefig(str(HILBERT_FIG_PATH / "hilbert_fig.png"), dpi=140)


if __name__ == "__main__":
    check_folder(HILBERT_FIG_PATH)
    hilbert_main()
