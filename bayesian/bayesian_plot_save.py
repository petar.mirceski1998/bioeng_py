import os
from pathlib import Path

import numpy as np
from processing.bayesian_inference import BayesianInference
from processing.coupling_plot import plot_coupling_function
from processing.utils.constants import BAYESIAN_PLOTS_PATH, PHASE_OUTPUT_PATH
from processing.utils.data_utils import check_folder, load_phases
from tqdm import tqdm

window_size = 57
overlap = 1
sampling_step = 1 / 100


def construct_name(file_path: Path) -> Path:
    file_path = file_path.with_suffix("")
    _, file_name = os.path.split(file_path)
    return Path(file_name)


def patient_bayes_main() -> None:
    FILE_PATHS = [path for path in PHASE_OUTPUT_PATH.rglob("*.pickle")]
    bayesian = BayesianInference(window_size, sampling_step, overlap)
    check_folder(BAYESIAN_PLOTS_PATH)
    for file_path in tqdm(FILE_PATHS):
        file_name = construct_name(file_path)
        first_phase, second_phase = load_phases(file_path)
        cc, _ = bayesian(first_phase, second_phase)
        if (not isinstance(cc, list)) or len(cc) == 0:
            continue

        cc = np.array(cc).mean(0)
        plot_coupling_function(
            cc,
            bayesian.fourier_base_order,
            file_name=BAYESIAN_PLOTS_PATH / file_name,
        )


if __name__ == "__main__":
    patient_bayes_main()
