import os
from typing import Dict

import numpy as np
import numpy.typing as npt
import pandas as pd
import pyedflib
from processing.utils.constants import (
    EDF_FILE_PATH,
    LEFT_HALF_PROBES,
    PATIENT_CSV_PATH,
    PROBE_PROCESSED_SIGNALS,
    RE_SAMPLE_FREQ,
    RIGHT_HALF_PROBES,
    SAMPLING_FREQ,
)
from processing.utils.data_utils import remove_and_create_folders, save_file
from scipy import signal
from tqdm import tqdm


def get_left_and_right_signals(
    file: pyedflib.EdfReader, n: int
) -> Dict[str, npt.NDArray[np.float64]]:
    shape_of_signal = int(file.getNSamples()[0] / SAMPLING_FREQ * RE_SAMPLE_FREQ)
    left_eeg = []
    right_eeg = []

    for i in np.arange(n):
        type_of_probe = file.signal_label(i).decode("utf-8").strip()[3:].strip()

        eeg_signal = file.readSignal(i)
        eeg_signal = signal.resample(eeg_signal, shape_of_signal)

        if type_of_probe in LEFT_HALF_PROBES:
            left_eeg.append(eeg_signal)

        if type_of_probe in RIGHT_HALF_PROBES:
            right_eeg.append(eeg_signal)

    assert len(left_eeg) == len(right_eeg)
    left_eeg_array: npt.NDArray[np.float64] = np.array(left_eeg).mean(axis=0)
    right_eeg_array: npt.NDArray[np.float64] = np.array(right_eeg).mean(axis=0)

    return {"left_eeg": left_eeg_array, "right_eeg": right_eeg_array}


def left_right_main() -> None:
    FILE_PATHS = (
        EDF_FILE_PATH
        / pd.read_csv(PATIENT_CSV_PATH, sep=" ", header=None).values.ravel()
    )

    remove_and_create_folders(PROBE_PROCESSED_SIGNALS)

    for path in tqdm(FILE_PATHS):
        file = pyedflib.EdfReader(str(path))  # read the file
        n = file.signals_in_file - 1  # read the number of eeg signals

        data = get_left_and_right_signals(file, n)
        # Construct the file names
        base, file_name = os.path.split(path)
        *_, freq_type = os.path.split(base)
        file_name = file_name.split(".")[0]
        location = PROBE_PROCESSED_SIGNALS / freq_type / file_name

        save_file(data, location)


if __name__ == "__main__":
    left_right_main()
