from pathlib import Path
from typing import List

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from processing.synchronization import phase_synchronization_index
from processing.utils.constants import (
    HISTOGRAM_SAVE_PATH,
    LOG_FILE_PATH,
    PHASE_OUTPUT_PATH,
)
from processing.utils.data_utils import check_folder, load_phases
from tqdm import tqdm


def save_histogram(synch_indexes: List[float], title: str = "") -> None:
    file_location = str(
        (HISTOGRAM_SAVE_PATH / title.replace(" ", "_")).with_suffix(".png")
    ).lower()

    plt.figure(figsize=(16.0, 10.0))
    plt.hist(synch_indexes, bins=50)
    plt.title(title)
    plt.xlabel("mean phase coherence index")

    if title == "":
        plt.show()
    else:
        plt.savefig(file_location, dpi=140)


def parse_data_failed(file_names: List[str]) -> List[float]:
    synch_indexes: List[float] = []
    for file_name in tqdm(file_names):
        first_phase, second_phase = load_phases(
            (PHASE_OUTPUT_PATH / file_name).with_suffix(".pickle")
        )
        first_phase = np.unwrap(first_phase)
        second_phase = np.unwrap(second_phase)

        difference = first_phase - second_phase
        synch_index = phase_synchronization_index(difference)
        synch_indexes.append(synch_index)
    return synch_indexes


def parse_data_all(file_names: List[Path]) -> List[float]:
    synch_indexes: List[float] = []
    for file_name in tqdm(file_names):
        first_phase, second_phase = load_phases(file_name)
        first_phase = np.unwrap(first_phase)
        second_phase = np.unwrap(second_phase)

        difference = first_phase - second_phase
        synch_index = phase_synchronization_index(difference)
        synch_indexes.append(synch_index)
    return synch_indexes


def main_parse() -> None:
    check_folder(HISTOGRAM_SAVE_PATH)
    log_phases: List[str] = []
    log_file_df = pd.read_csv(
        LOG_FILE_PATH, header=None, sep=" ", names=["log_type", "combination_name"]
    )
    log_phases = log_file_df["combination_name"]

    all_phases = [path for path in PHASE_OUTPUT_PATH.rglob("*.pickle")]

    synch_indexes_failed = parse_data_failed(log_phases)
    synch_indexes_all = parse_data_all(all_phases)

    save_histogram(
        synch_indexes_failed, "Mean phase coherence index of failed subjects"
    )

    save_histogram(synch_indexes_all, "Mean phase coherence index of all subjects")


if __name__ == "__main__":
    main_parse()
