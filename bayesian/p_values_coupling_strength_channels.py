import json
import pickle
from itertools import combinations
from pathlib import Path
from typing import Dict, List, Tuple

import numpy as np
import numpy.typing as npt
from processing.net_coupling import dirc
from processing.utils.constants import (
    ALL_COMBINATIONS,
    BAYESIAN_DATA_PATH,
    CHANNELS,
    P_VALUES_COMBINATION_FOLDER,
)
from processing.utils.data_utils import check_folder, get_channel_combination
from scipy.stats import ranksums

CHANNELS = [channel.replace("EEG", "").strip() for channel in CHANNELS]
experiment_codes: Dict[int, str] = {0: "5Hz", 1: "6Hz", 2: "10Hz"}
experiment_combinations: List[Tuple[int, int]] = [
    (comb[0], comb[1]) for comb in combinations(range(3), 2)
]


def load_data(file_path: Path) -> npt.NDArray[np.float64]:
    patient = pickle.loads(file_path.read_bytes())
    data: npt.NDArray[np.float64] = np.array(patient["cc"])
    return np.array(data.mean(axis=0))


def p_test(
    coupling_data: List[np.ndarray],
    combination: Tuple[Tuple[int, int], Tuple[int, int]],
) -> float:

    wilcoxon_result = ranksums(
        coupling_data[0],
        coupling_data[1],
    )
    print(
        f" pvalue between wavelet power {combination[0]} {combination[1]}: ",
        wilcoxon_result.pvalue,
    )
    return float(wilcoxon_result.pvalue)


def calculate_coupling_strength(
    file_path: Path,
) -> Tuple[float, float, Tuple[int, int]]:
    combination = get_channel_combination(str(file_path))
    cc = load_data(file_path)
    cpl_1, cpl_2, _ = dirc(cc)
    return cpl_1, cpl_2, combination


# TODO: THIS IS NOT GOOD WE NEED THIS FOR EVERY PATIENT D:
def mean_freq_map_main() -> None:
    check_folder(P_VALUES_COMBINATION_FOLDER)
    experiment_combination: Dict[Tuple[int, int], List[Tuple[float, float]]] = {
        key: [] for key in ALL_COMBINATIONS
    }

    data_files = [file for file in BAYESIAN_DATA_PATH.rglob("*.pickle")]

    for file_path in data_files:
        cpl_1, cpl_2, combination = calculate_coupling_strength(file_path)
        if cpl_1 < 10000 or cpl_2 < 10000:
            experiment_combination[combination].append((cpl_1, cpl_2))

    for first_combination_index in range(len(ALL_COMBINATIONS)):
        p_value_json_data: Dict[str, float] = {}
        for second_combination_index in range(
            first_combination_index, len(ALL_COMBINATIONS)
        ):
            first_combination = ALL_COMBINATIONS[first_combination_index]
            second_combination = ALL_COMBINATIONS[second_combination_index]
            if first_combination == second_combination:
                continue

            for i in range(2):
                first_combination_strength = np.array(
                    experiment_combination[first_combination]
                )[:, i]
                second_combination_strength = np.array(
                    experiment_combination[second_combination]
                )[:, i]
                p_value = p_test(
                    [first_combination_strength, second_combination_strength],
                    (first_combination, second_combination),
                )
                p_value_json_data[
                    f"{first_combination}{second_combination}{i}"
                ] = p_value

        # REVIEW: Rewrite this with pathlib
        with open(
            str(
                P_VALUES_COMBINATION_FOLDER
                / f"pvalues_channel_experiment({first_combination}).json"
            ),
            "w",
        ) as outfile:
            json.dump(p_value_json_data, outfile)


if __name__ == "__main__":
    mean_freq_map_main()
