import copy
import json
import pickle
from itertools import combinations
from pathlib import Path
from typing import Dict, List, Tuple

import numpy as np
import numpy.typing as npt
from processing.net_coupling import dirc
from processing.utils.constants import (
    ALL_COMBINATIONS,
    BAYESIAN_DATA_PATH,
    CHANNELS,
    P_VALUES_FREQ_FOLDER,
)
from processing.utils.data_utils import check_folder, get_channel_combination
from scipy.stats import ranksums

CHANNELS = [channel.replace("EEG", "").strip() for channel in CHANNELS]
experiment_codes: Dict[int, str] = {0: "5Hz", 1: "6Hz", 2: "10Hz"}
experiment_combinations: List[Tuple[int, int]] = [
    (comb[0], comb[1]) for comb in combinations(range(3), 2)
]


def load_data(file_path: Path) -> npt.NDArray[np.float64]:
    patient = pickle.loads(file_path.read_bytes())
    data: npt.NDArray[np.float64] = np.array(patient["cc"])
    return np.array(data.mean(axis=0))


def p_test(coupling_data: List[np.ndarray]) -> Dict[str, float]:
    p_values_for_freq: Dict[str, float] = {}

    for first_experiment_index, second_experiment_index in experiment_combinations:
        wilcoxon_result = ranksums(
            coupling_data[first_experiment_index],
            coupling_data[second_experiment_index],
        )
        print(
            f" pvalue between wavelet power {experiment_codes[first_experiment_index]} {experiment_codes[second_experiment_index]}: ",
            wilcoxon_result.pvalue,
        )
        tuple_combination = (
            experiment_codes[first_experiment_index],
            experiment_codes[second_experiment_index],
        )
        key_combination = "".join(tuple_combination)
        p_values_for_freq[key_combination] = float(wilcoxon_result.pvalue)

    return p_values_for_freq


def calculate_coupling_strength(
    file_path: Path,
) -> Tuple[float, float, Tuple[int, int]]:
    combination = get_channel_combination(str(file_path))
    cc = load_data(file_path)
    cpl_1, cpl_2, _ = dirc(cc)
    return cpl_1, cpl_2, combination


# TODO: THIS IS NOT GOOD WE NEED THIS FOR EVERY PATIENT D:
def mean_freq_map_main() -> None:
    check_folder(P_VALUES_FREQ_FOLDER)
    combination_dict_list: Dict[Tuple[int, int], List[Tuple[float, float]]] = {
        key: [] for key in ALL_COMBINATIONS
    }

    experiment_combination = {
        "5Hz": copy.deepcopy(combination_dict_list),
        "6Hz": copy.deepcopy(combination_dict_list),
        "10Hz": copy.deepcopy(combination_dict_list),
    }

    p_value_json_data: Dict[str, Dict[str, float]] = {}

    data_files = [file for file in BAYESIAN_DATA_PATH.rglob("*.pickle")]

    five_hz = [file_path for file_path in data_files if "5Hz" in str(file_path)]
    six_hz = [file_path for file_path in data_files if "6Hz" in str(file_path)]
    ten_hz = [file_path for file_path in data_files if "10Hz" in str(file_path)]

    assert len(five_hz) + len(ten_hz) + len(six_hz) == len(data_files)

    # REVIEW: Does this have to be repeated three times ?
    for file_path in five_hz:
        cpl_1, cpl_2, combination = calculate_coupling_strength(file_path)
        if cpl_1 < 10000 or cpl_2 < 10000:
            experiment_combination["5Hz"][combination].append((cpl_1, cpl_2))

    for file_path in six_hz:
        cpl_1, cpl_2, combination = calculate_coupling_strength(file_path)
        if cpl_1 < 10000 or cpl_2 < 10000:
            experiment_combination["6Hz"][combination].append((cpl_1, cpl_2))

    for file_path in ten_hz:
        cpl_1, cpl_2, combination = calculate_coupling_strength(file_path)
        if cpl_1 < 10000 or cpl_2 < 10000:
            experiment_combination["10Hz"][combination].append((cpl_1, cpl_2))

    for combination in combination_dict_list:
        five_hz_strength = np.array(experiment_combination["5Hz"][combination])
        six_hz_strength = np.array(experiment_combination["6Hz"][combination])
        ten_hz_strength = np.array(experiment_combination["10Hz"][combination])
        for i in range(2):
            five_hz_coupling = five_hz_strength[:, i]
            six_hz_coupling = six_hz_strength[:, i]
            ten_hz_coupling = ten_hz_strength[:, i]
            p_value_dict = p_test([five_hz_coupling, six_hz_coupling, ten_hz_coupling])
            p_value_json_data[f"{combination}{i}"] = p_value_dict

    with open(
        str(P_VALUES_FREQ_FOLDER / "pvalues_freq_experiment.json"), "w"
    ) as outfile:
        json.dump(p_value_json_data, outfile)


if __name__ == "__main__":
    mean_freq_map_main()
