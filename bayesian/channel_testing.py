import pandas as pd
import pyedflib
from tqdm import tqdm

from processing.utils.constants import CHANNELS, EDF_FILE_PATH, PATIENT_CSV_PATH

# THESE ARE THE CHANNELS ORDERED BY THEIR INDEX OF THE ELEMENT
ERROR_MESSAGE = """CHANNELS ARENT THE WAY PRESENTED
                   SEE THE CONSTANT IN THE FILE NAME"""


def main() -> None:
    FILE_PATHS = (
        EDF_FILE_PATH
        / pd.read_csv(PATIENT_CSV_PATH, sep=" ", header=None).values.ravel()
    )

    for path in tqdm(FILE_PATHS):
        file = pyedflib.EdfReader(str(path))  # read the file
        n = file.signals_in_file - 1  # read the number of eeg signals
        list_of_channels = []
        for i in range(n):
            list_of_channels.append(file.signal_label(i).decode("utf-8").strip())

        if CHANNELS != list_of_channels:
            raise Exception(ERROR_MESSAGE)

    print("\nTHE TEST WAS SUCCESSFULL \n")
    print("THE CHANNEL LIST IS:\n ", CHANNELS)
    print("\n")


if __name__ == "__main__":
    main()
