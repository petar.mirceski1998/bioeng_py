from typing import Dict, Tuple

import numpy as np
from tqdm import tqdm

from processing.coupling_plot import plot_frequency_mean_coupling_function
from processing.mean_parameter_calculation import mean_parameter_calculation
from processing.utils.constants import BAYESIAN_DATA_PATH, MEAN_FREQUENCY_PLOT_PATH
from processing.utils.data_utils import check_folder


def save_data_plot(
    mean_10: Dict[Tuple[int, int], np.ndarray],
    mean_6: Dict[Tuple[int, int], np.ndarray],
    mean_5: Dict[Tuple[int, int], np.ndarray],
) -> None:
    check_folder(MEAN_FREQUENCY_PLOT_PATH)
    for combination, mean_cc_10 in tqdm(mean_10.items()):
        mean_cc_6 = mean_6[combination]
        mean_cc_5 = mean_5[combination]
        save_file_name = (
            MEAN_FREQUENCY_PLOT_PATH / str(combination).replace(" ", "").strip()
        )
        plot_frequency_mean_coupling_function(
            mean_cc_10,
            mean_cc_6,
            mean_cc_5,
            combination=combination,
            file_name=save_file_name,
        )


def main_mean_freq() -> None:
    file_paths = [path for path in BAYESIAN_DATA_PATH.rglob("*.pickle")]

    file_paths_10 = [path for path in file_paths if "10Hz" in str(path)]
    file_paths_6 = [path for path in file_paths if "6Hz" in str(path)]
    file_paths_5 = [path for path in file_paths if "5Hz" in str(path)]

    experiment_combination_10 = mean_parameter_calculation(file_paths_10)
    experiment_combination_6 = mean_parameter_calculation(file_paths_6)
    experiment_combination_5 = mean_parameter_calculation(file_paths_5)

    save_data_plot(
        experiment_combination_10, experiment_combination_6, experiment_combination_5,
    )


if __name__ == "__main__":
    main_mean_freq()
    pass
