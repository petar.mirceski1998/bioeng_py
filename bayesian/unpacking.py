from pathlib import Path
from typing import Dict

import numpy as np
import numpy.typing as npt
import pandas as pd
import pyedflib
from processing.utils.constants import (
    EDF_FILE_PATH,
    PATIENT_CSV_PATH,
    RE_SAMPLE_FREQ,
    SAMPLING_FREQ,
    UNPACKED_SIGNALS_PATH,
)
from processing.utils.data_utils import remove_and_create_folders, save_file
from scipy import signal
from tqdm import tqdm


def read_edf_file(
    file: pyedflib.EdfReader, n: int
) -> Dict[str, npt.NDArray[np.float64]]:
    n_samples_per_dimenstion = file.getNSamples()
    assert np.all(n_samples_per_dimenstion == n_samples_per_dimenstion[0])
    shape_of_signal = int(n_samples_per_dimenstion[0] / SAMPLING_FREQ * RE_SAMPLE_FREQ)
    sigbufs = np.zeros((n, shape_of_signal))
    for i in range(n):
        eeg_signal = file.readSignal(i)
        resampled_signal = signal.resample(eeg_signal, shape_of_signal)
        sigbufs[i, :] = resampled_signal  # fill the empty array

    return {"eeg": sigbufs}


def construct_save_path(path: Path) -> Path:
    file_name = path.stem
    freq_type = path.parts[-2]
    return Path(UNPACKED_SIGNALS_PATH / freq_type / file_name).with_suffix(".pickle")


def main_unpack() -> None:
    remove_and_create_folders(UNPACKED_SIGNALS_PATH)
    record_names = pd.read_csv(PATIENT_CSV_PATH, sep=" ", header=None).values.ravel()
    file_paths = EDF_FILE_PATH / record_names

    for path in tqdm(file_paths):
        file = pyedflib.EdfReader(str(path))  # read the file
        # minus one because of control signal look this up in the data description
        number_of_channels = file.signals_in_file - 1

        # read the data and construct the dictionary
        data = read_edf_file(file, number_of_channels)
        location = construct_save_path(path)
        save_file(data, location)


if __name__ == "__main__":
    main_unpack()
