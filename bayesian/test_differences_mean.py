import math
import os
import pickle
from pathlib import Path
from typing import Optional, Tuple

import numpy as np
import numpy.typing as npt
from scipy.io import loadmat
from tqdm import tqdm

PICKLE_PATH = Path("./output_data/bayesian/data")
MAT_PATH = Path("./bayes_data")


def load_file_pickle(file_path: Path) -> npt.NDArray[np.float64]:
    data = pickle.load(file_path.read_bytes(), encoding="utf-8")
    return np.array(data["cc"])


def load_file_mat(file_path: Path) -> npt.NDArray[np.float64]:
    data = loadmat(str(file_path))
    return np.array(data["cc"])


def load_file(file_path: str) -> Optional[Tuple[np.ndarray, np.ndarray]]:
    mat_file = (MAT_PATH / file_path).with_suffix(".mat")
    pickle_file = (PICKLE_PATH / file_path.replace("-", ",")).with_suffix(".pickle")

    if os.path.exists(mat_file) and os.path.exists(pickle_file):
        return load_file_mat(mat_file), load_file_pickle(pickle_file)

    return None


def compare_main(cc_mat: np.ndarray, cc_pickle: np.ndarray) -> float:
    min_dim = min(cc_mat.shape[0], cc_pickle.shape[0])
    mean_abs_diff = []
    for i in range(min_dim):
        mean_abs_diff.append(np.mean(np.abs(cc_mat[i] - cc_pickle[i])))

    return float(np.mean(mean_abs_diff))


if __name__ == "__main__":
    files = [
        str(file_path.with_suffix("").name).split("/")[-1]
        for file_path in MAT_PATH.rglob("*.mat")
    ]
    avg = []
    for file_path in tqdm(files):
        params = load_file(file_path)
        if params is not None:
            cc_mat, cc_pickle = params
            mad = compare_main(cc_mat, cc_pickle)
            avg.append(mad)
    avg = [element for element in avg if not math.isnan(element)]
    print(avg)
    print(np.mean(np.array(avg)))
