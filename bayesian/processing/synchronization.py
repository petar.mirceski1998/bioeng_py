import numpy as np
import numpy.typing as npt


def phase_synchronization_index(phase_difference: npt.NDArray[np.float64]) -> float:
    """Calculates the mean phase coherence index"""
    return float(np.abs(synchronization_calculation(phase_difference)))


def synchronization_angle(phase_difference: npt.NDArray[np.float64]) -> float:
    """Calculates the angle of mean phase coherence"""
    return float(np.angle(synchronization_calculation(phase_difference)))


def synchronization_calculation(phase_difference: npt.NDArray[np.float64]) -> complex:
    """Calculates the complex exponention of the phase differences"""
    return complex(
        (1 / phase_difference.shape[1])
        * np.sum(np.exp(complex(0, 1) * phase_difference))
    )
