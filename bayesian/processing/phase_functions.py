from typing import Tuple

import numpy as np
import numpy.typing as npt
import scipy.signal


# TODO: REMOVE THE UNWANTED COORDINATES
def co_hilbproto(
    signal: npt.NDArray[np.float64],
    ntail: int = 1000,
) -> npt.NDArray[np.float64]:

    """Input:  x is scalar timeseries,
                ntail  is the number of points at the ends to be cut off,
                   by default ntail=1000
    Output: theta is the protophase in 0,2pi interval
    """
    hilbert_tf = scipy.signal.hilbert(signal)
    hilbert_tf = hilbert_tf[ntail : signal.shape[0] - ntail]
    hilbert_tf = hilbert_tf - np.mean(hilbert_tf)
    theta = np.angle(hilbert_tf)
    theta = np.remainder(theta, 2 * np.pi)
    return np.array(theta, dtype=np.float64)


def co_fbtransf1(
    theta: np.ndarray, nfft: int = 80, alpha: float = 0.05, ngrid: int = 50
) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """
    Fourier series based transformation
    protophase theta --> phase phi for one oscillator.
    Input parameters:
        theta:  protophase
        nfft:   max number of Fourier harmonics,
                default value is 80
        alpha: smoothing coefficient, by default alpha=0.05
    Output:
            [phi,arg,sigma] = co_fbtransf1(...) if also the transformation
            function sigma is required; it can be plotted as
            plot(arg,sigma); sigma is computed on the grid.
            Default grid size is 50.
    """
    Spl = np.zeros(shape=(nfft, 1), dtype=np.complex64)
    al2 = alpha ** 2
    npt = theta.shape[0]

    for i in range(1, nfft + 1):
        Spl[i - 1] = np.sum(np.exp(-1j * i * theta)) / npt

    phi = np.copy(theta).astype(np.float64)
    arg = np.arange(0, ngrid).reshape(1, -1)
    arg = arg * np.pi * 2 / (ngrid - 1)
    arg = arg.T
    sigma = np.ones(shape=(ngrid, 1))
    for i in range(1, nfft + 1):
        kernel = np.exp(-0.5 * i ** 2 * al2)
        sigma = sigma + kernel * 2 * np.real(Spl[i - 1] * np.exp(1j * i * arg))
        phi = phi + kernel * 2 * np.imag(Spl[i - 1] * (np.exp(1j * i * theta) - 1) / i)

    return phi, arg, sigma


def series_to_phase(
    first_signal: npt.NDArray[np.float64], second_signal: npt.NDArray[np.float64]
) -> Tuple[npt.NDArray[np.float64], npt.NDArray[np.float64]]:

    first_signal_hilbert = co_hilbproto(first_signal, 1000)
    first_signal_phase, _, _ = co_fbtransf1(first_signal_hilbert)
    first_signal_phase = np.unwrap(first_signal_phase)

    second_signal_hilbert = co_hilbproto(second_signal, 1000)
    second_signal_phase, _, _ = co_fbtransf1(second_signal_hilbert)
    second_signal_phase = np.unwrap(second_signal_phase)

    return (first_signal_phase.reshape(1, -1), second_signal_phase.reshape(1, -1))
