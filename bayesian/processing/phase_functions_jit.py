from typing import Tuple

import numpy as np
from numba import njit


@njit  # type:ignore
def co_fbtransf1(
    theta: np.ndarray, nfft: int = 80, alpha: float = 0.05, ngrid: int = 50
) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """
    Fourier series based transformation
    protophase theta --> phase phi for one oscillator.
    Input parameters:
        theta:  protophase
        nfft:   max number of Fourier harmonics,
                default value is 80
        alpha: smoothing coefficient, by default alpha=0.05
    Output:
            [phi,arg,sigma] = co_fbtransf1(...) if also the transformation
            function sigma is required; it can be plotted as
            plot(arg,sigma); sigma is computed on the grid.
            Default grid size is 50.
    """
    Spl = np.zeros(shape=(nfft, 1), dtype=np.complex64)
    al2 = alpha ** 2
    npt = theta.shape[0]

    for i in range(1, nfft + 1):
        Spl[i - 1] = np.sum(np.exp(-1j * i * theta)) / npt

    phi = np.copy(theta).astype(np.float64)
    arg = np.arange(0, ngrid).reshape(1, -1)
    arg = arg * np.pi * 2 / (ngrid - 1)
    arg = arg.T
    sigma = np.ones(shape=(ngrid, 1))
    for i in range(1, nfft + 1):
        kernel = np.exp(-0.5 * i ** 2 * al2)
        sigma = sigma + kernel * 2 * np.real(Spl[i - 1] * np.exp(1j * i * arg))
        phi = phi + kernel * 2 * np.imag(Spl[i - 1] * (np.exp(1j * i * theta) - 1) / i)

    return phi, arg, sigma
