from typing import Tuple

import numpy as np

from processing.utils.array_manipulation import _norm


def dirc(c: np.ndarray, bn: int = 2) -> Tuple[float, float, float]:
    q1 = np.zeros((c.shape[0] // 2) - 1, dtype=np.double)
    q2 = np.zeros((c.shape[0] // 2) - 1, dtype=np.double)
    iq1 = 0
    iq2 = 0
    br = 1
    K = int(c.shape[0] / 2)

    for i in range(bn):
        q1[iq1] = c[br]
        iq1 += 1
        q1[iq1] = c[br + 1]
        iq1 += 1
        q2[iq2] = c[K + br]
        iq2 += 1
        q2[iq2] = c[K + br + 1]
        iq2 += 1
        br += 2

    for i in range(bn):
        q1[iq1] = c[br]
        iq1 += 1
        q1[iq1] = c[br + 1]
        iq1 += 1
        q2[iq2] = c[K + br]
        iq2 += 1
        q2[iq2] = c[K + br + 1]
        iq2 += 1
        br += 2

    for ii in range(bn):
        for jj in range(bn):
            q1[iq1] = c[br]
            iq1 += 1
            q1[iq1] = c[br + 1]
            iq1 += 1

            q2[iq2] = c[K + br]
            iq2 += 1
            q2[iq2] = c[K + br + 1]
            iq2 += 1

            br += 2

            q1[iq1] = c[br]
            iq1 += 1
            q1[iq1] = c[br + 1]
            iq1 += 1

            q2[iq2] = c[K + br]
            iq2 += 1
            q2[iq2] = c[K + br + 1]
            iq2 += 1

            br += 2

    assert q1.shape[0] == q2.shape[0]

    cpl_1 = _norm(q1)
    cpl_2 = _norm(q2)
    drc = (cpl_2 - cpl_1) / (cpl_1 + cpl_2)
    return cpl_1, cpl_2, drc
