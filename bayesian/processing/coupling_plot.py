from pathlib import Path
from typing import Optional, Tuple, Union

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
from processing.utils.constants import CHANNELS

FIG_SIZE = (20.0, 10.0)
FIG_SIZE_MEAN_FREQ = (20.0, 20.0)
FIG_SIZE_MEAN_STATE = (20.0, 15.0)


def ax_util_labels(ax: Axes3D, first_channel: str, second_channel: str) -> None:
    pad = 5
    _transparent_plot_map(ax)
    ax.annotate(
        first_channel,
        xy=(0, 0.5),
        xytext=(ax.xaxis.labelpad - pad, -4 * pad),
        xycoords=ax.xaxis.label,
        textcoords="offset points",
        size="large",
        ha="right",
        va="center",
    )
    ax.annotate(
        second_channel,
        xy=(0, 0.5),
        xytext=(ax.yaxis.labelpad - pad, -pad),
        xycoords=ax.yaxis.label,
        textcoords="offset points",
        size="large",
        ha="right",
        va="center",
    )


def construct_coupling_function(
    cc: np.ndarray, fourier_base_order: int
) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    t = np.arange(0, 2 * np.pi, 0.13)
    q1 = np.zeros(shape=(t.shape[0], t.shape[0]))
    q2 = q1.copy()

    K = len(cc) // 2

    for i in range(len(t)):
        for j in range(len(t)):
            br = 1
            for idx in range(1, fourier_base_order + 1):
                q1[i, j] = (
                    q1[i, j]
                    + cc[br] * np.sin(idx * t[i])
                    + cc[br + 1] * np.cos(idx * t[i])
                )
                q2[i, j] = (
                    q2[i, j]
                    + cc[K + br] * np.sin(idx * t[j])
                    + cc[K + br + 1] * np.cos(idx * t[j])
                )
                br += 2

            for idx in range(1, fourier_base_order + 1):
                q1[i, j] = (
                    q1[i, j]
                    + cc[br] * np.sin(idx * t[j])
                    + cc[br + 1] * np.cos(idx * t[j])
                )
                q2[i, j] = (
                    q2[i, j]
                    + cc[K + br] * np.sin(idx * t[i])
                    + cc[K + br + 1] * np.cos(idx * t[i])
                )
                br += 2

            for ii in range(1, fourier_base_order + 1):
                for jj in range(1, fourier_base_order + 1):
                    phase_sum = ii * t[i] + jj * t[j]
                    phase_difference = ii * t[i] - jj * t[j]

                    q1[i, j] = (
                        q1[i, j]
                        + cc[br] * np.sin(phase_sum)
                        + cc[br + 1] * np.cos(phase_sum)
                    )
                    q2[i, j] = (
                        q2[i, j]
                        + cc[K + br] * np.sin(phase_sum)
                        + cc[K + br + 1] * np.cos(phase_sum)
                    )

                    br += 2

                    q1[i, j] = (
                        q1[i, j]
                        + cc[br] * np.sin(phase_difference)
                        + cc[br + 1] * np.cos(phase_difference)
                    )
                    q2[i, j] = (
                        q2[i, j]
                        + cc[K + br] * np.sin(-phase_difference)
                        + cc[K + br + 1] * np.cos(-phase_difference)
                    )
                    br += 2

    return q1.T, q2, t


def _transparent_plot_map(ax: matplotlib.axes) -> None:
    ax.xaxis._axinfo["grid"]["color"] = (1, 1, 1, 0)
    ax.yaxis._axinfo["grid"]["color"] = (1, 1, 1, 0)
    ax.zaxis._axinfo["grid"]["color"] = (1, 1, 1, 0)
    ax.view_init(elev=50, azim=-125)


def plot_coupling_function(
    cc: np.ndarray,
    fourier_base_order: int = 2,
    file_name: Optional[Union[str, Path]] = None,
) -> None:
    q1, q2, t = construct_coupling_function(cc, fourier_base_order)
    T1, T2 = np.meshgrid(t.copy(), t.copy())

    fig = plt.figure(figsize=FIG_SIZE)

    ax = fig.add_subplot(1, 2, 1, projection="3d")
    ax.plot_surface(T1, T2, q1, cmap=matplotlib.cm.hot)
    _transparent_plot_map(ax)

    ax = fig.add_subplot(1, 2, 2, projection="3d")
    ax.plot_surface(T1, T2, q2, cmap=matplotlib.cm.hot)
    _transparent_plot_map(ax)

    if file_name is not None:
        file_name = Path(file_name)
        if file_name.suffix == "":
            file_name = file_name.with_suffix(".png")

        plt.savefig(str(file_name))
    else:
        plt.show()

    plt.close()


def plot_state_mean_coupling_function(
    cc_a: np.ndarray,
    cc_b: np.ndarray,
    combination: Tuple[int, int],
    fourier_base_order: int = 2,
    file_name: Optional[Union[str, Path]] = None,
) -> None:
    first_channel = CHANNELS[combination[0] - 1]
    second_channel = CHANNELS[combination[1] - 1]
    pad = 5

    q1_a, q2_a, t = construct_coupling_function(cc_a, fourier_base_order)
    q1_b, q2_b, t = construct_coupling_function(cc_b, fourier_base_order)
    T1, T2 = np.meshgrid(t.copy(), t.copy())

    fig = plt.figure(figsize=FIG_SIZE_MEAN_STATE)

    ax = fig.add_subplot(2, 2, 1, projection="3d")
    ax.plot_surface(T1, T2, q1_a, cmap=matplotlib.cm.hot)
    ax_util_labels(ax, first_channel, second_channel)
    ax.annotate(
        "First Experiment",
        xy=(0, 0.5),
        xytext=(-ax.zaxis.labelpad - pad, 0),
        xycoords=ax.zaxis.label,
        textcoords="offset points",
        size="large",
        ha="right",
        va="center",
    )

    ax = fig.add_subplot(2, 2, 2, projection="3d")
    ax.plot_surface(T1, T2, q2_a, cmap=matplotlib.cm.hot)
    ax_util_labels(ax, second_channel, first_channel)

    ax = fig.add_subplot(2, 2, 3, projection="3d")
    ax.plot_surface(T1, T2, q1_b, cmap=matplotlib.cm.hot)
    ax_util_labels(ax, first_channel, second_channel)
    ax.annotate(
        "Second Experiment",
        xy=(0, 0.5),
        xytext=(-ax.zaxis.labelpad - pad, 0),
        xycoords=ax.zaxis.label,
        textcoords="offset points",
        size="large",
        ha="right",
        va="center",
    )

    ax = fig.add_subplot(2, 2, 4, projection="3d")
    ax.plot_surface(T1, T2, q2_b, cmap=matplotlib.cm.hot)
    ax_util_labels(ax, second_channel, first_channel)

    if file_name is not None:
        file_name = Path(file_name)
        if file_name.suffix == "":
            file_name = file_name.with_suffix(".png")

        plt.savefig(str(file_name))
    else:
        plt.show()

    plt.close()


# TODO: FINISH THIS
def plot_frequency_mean_coupling_function(
    mean_cc_10: np.ndarray,
    mean_cc_6: np.ndarray,
    mean_cc_5: np.ndarray,
    combination: Tuple[int, int],
    fourier_base_order: int = 2,
    file_name: Optional[Union[str, Path]] = None,
) -> None:
    first_channel = CHANNELS[combination[0] - 1]
    second_channel = CHANNELS[combination[1] - 1]
    pad = 5

    q1_10, q2_10, t = construct_coupling_function(mean_cc_10, fourier_base_order)
    q1_6, q2_6, t = construct_coupling_function(mean_cc_6, fourier_base_order)
    q1_5, q2_5, t = construct_coupling_function(mean_cc_5, fourier_base_order)

    T1, T2 = np.meshgrid(t.copy(), t.copy())

    fig = plt.figure(figsize=FIG_SIZE_MEAN_FREQ)

    ax = fig.add_subplot(3, 2, 1, projection="3d")
    ax.plot_surface(T1, T2, q1_10, cmap=matplotlib.cm.hot)
    ax_util_labels(ax, first_channel, second_channel)
    ax.annotate(
        "10Hz Experiment",
        xy=(0, 0.5),
        xytext=(-ax.zaxis.labelpad - pad, 0),
        xycoords=ax.zaxis.label,
        textcoords="offset points",
        size="large",
        ha="right",
        va="center",
    )

    ax = fig.add_subplot(3, 2, 2, projection="3d")
    ax.plot_surface(T1, T2, q2_10, cmap=matplotlib.cm.hot)
    ax_util_labels(ax, second_channel, first_channel)

    ax = fig.add_subplot(3, 2, 3, projection="3d")
    ax.plot_surface(T1, T2, q1_6, cmap=matplotlib.cm.hot)
    ax_util_labels(ax, first_channel, second_channel)
    ax.annotate(
        "6Hz Experiment",
        xy=(0, 0.5),
        xytext=(-ax.zaxis.labelpad - pad, 0),
        xycoords=ax.zaxis.label,
        textcoords="offset points",
        size="large",
        ha="right",
        va="center",
    )

    ax = fig.add_subplot(3, 2, 4, projection="3d")
    ax.plot_surface(T1, T2, q2_6, cmap=matplotlib.cm.hot)
    ax_util_labels(ax, second_channel, first_channel)

    ax = fig.add_subplot(3, 2, 5, projection="3d")
    ax.plot_surface(T1, T2, q1_5, cmap=matplotlib.cm.hot)
    ax_util_labels(ax, first_channel, second_channel)
    ax.annotate(
        "5Hz Experiment",
        xy=(0, 0.5),
        xytext=(-ax.zaxis.labelpad - pad, 0),
        xycoords=ax.zaxis.label,
        textcoords="offset points",
        size="large",
        ha="right",
        va="center",
    )

    ax = fig.add_subplot(3, 2, 6, projection="3d")
    ax.plot_surface(T1, T2, q2_5, cmap=matplotlib.cm.hot)
    ax_util_labels(ax, second_channel, first_channel)
    if file_name is not None:
        file_name = Path(file_name)
        file_name = file_name.with_suffix(".png")
        plt.savefig(str(file_name))
    else:
        plt.show()

    plt.close()
