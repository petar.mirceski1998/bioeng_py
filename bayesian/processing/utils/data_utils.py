import logging
import os
import pickle
import re
import shutil
from pathlib import Path
from typing import Dict, Tuple

import numpy as np
import numpy.typing as npt
from processing.utils.constants import (
    LOG_FILE_PATH,
    PROBE_COMBINATION_REGEX,
    UNPACK_FOLDERS,
)


def remove_and_create_folders(processed_signals: Path) -> None:
    if os.path.isdir(processed_signals):
        shutil.rmtree(processed_signals)
    for folder in UNPACK_FOLDERS:
        result_folder = processed_signals / folder
        result_folder.mkdir(parents=True, exist_ok=True)


def save_file(data: Dict[str, np.ndarray], location: Path) -> None:
    """Saves the data to a pickle format"""
    location.write_bytes(pickle.dumps(data))


def load_phases(
    file_path: Path,
) -> Tuple[npt.NDArray[np.float64], npt.NDArray[np.float64]]:
    patient = pickle.loads(file_path.read_bytes())
    phases = tuple(phase.reshape(1, -1) for _, phase in patient.items())
    return phases[0], phases[1]


def load_patient(file_path: Path) -> npt.NDArray[np.float64]:
    patient = pickle.loads(file_path.read_bytes())
    return np.array(patient["eeg"])


def check_folder(path: Path) -> None:
    if os.path.isdir(path):
        shutil.rmtree(path)
    path.mkdir(parents=True, exist_ok=True)


def get_channel_combination(file: str) -> Tuple[int, int]:
    combination = re.findall(PROBE_COMBINATION_REGEX, file)[0]
    combination = combination.split(",")
    first = int(combination[0])
    second = int(combination[1])
    return first, second


def setup_logging() -> None:
    if os.path.exists(LOG_FILE_PATH):
        os.remove(LOG_FILE_PATH)
    logging.basicConfig(filename=LOG_FILE_PATH, level=logging.DEBUG)
