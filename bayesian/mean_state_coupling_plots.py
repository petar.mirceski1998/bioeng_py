import os
from typing import Dict, Tuple

import numpy as np
from tqdm import tqdm

from processing.coupling_plot import plot_state_mean_coupling_function
from processing.mean_parameter_calculation import mean_parameter_calculation
from processing.utils.constants import BAYESIAN_DATA_PATH, MEAN_STATE_PLOT_PATH
from processing.utils.data_utils import check_folder


def save_data_plot(
    mean_parameters_state_a: Dict[Tuple[int, int], np.ndarray],
    mean_parameters_state_b: Dict[Tuple[int, int], np.ndarray],
) -> None:

    check_folder(MEAN_STATE_PLOT_PATH)
    for combination, mean_cc_a in tqdm(mean_parameters_state_a.items()):
        mean_cc_b = mean_parameters_state_b[combination]
        save_file_name = (
            MEAN_STATE_PLOT_PATH / str(combination).replace(" ", "").strip()
        )
        plot_state_mean_coupling_function(
            mean_cc_a, mean_cc_b, combination=combination, file_name=save_file_name
        )


def main_state_freq() -> None:
    file_paths = [path for path in BAYESIAN_DATA_PATH.rglob("*.pickle")]

    file_paths_a_combinations = [
        path for path in file_paths if "a" in str(path).split(os.path.sep)[-1]
    ]
    file_paths_b_combinations = [
        path for path in file_paths if "b" in str(path).split(os.path.sep)[-1]
    ]

    experiment_a = mean_parameter_calculation(file_paths_a_combinations)
    experiment_b = mean_parameter_calculation(file_paths_b_combinations)

    save_data_plot(experiment_a, experiment_b)


if __name__ == "__main__":
    main_state_freq()
