import copy
import json
import pickle
from itertools import combinations
from pathlib import Path
from typing import Dict, List, Tuple

import numpy as np
import numpy.typing as npt
from processing.net_coupling import dirc
from processing.utils.constants import (
    ALL_COMBINATIONS,
    BAYESIAN_DATA_PATH,
    CHANNELS,
    P_VALUES_STATE_FOLDER,
)
from processing.utils.data_utils import check_folder, get_channel_combination
from scipy.stats import ranksums

CHANNELS = [channel.replace("EEG", "").strip() for channel in CHANNELS]
experiment_codes: Dict[int, str] = {0: "5Hz", 1: "6Hz", 2: "10Hz"}
experiment_combinations: List[Tuple[int, int]] = [
    (comb[0], comb[1]) for comb in combinations(range(3), 2)
]


def load_data(file_path: Path) -> npt.NDArray[np.float64]:
    patient = pickle.loads(file_path.read_bytes())
    data: npt.NDArray[np.float64] = np.array(patient["cc"])
    return np.array(data.mean(axis=0))


def p_test(
    coupling_data: List[np.ndarray],
    combination: Tuple[int, int],
) -> float:
    wilcoxon_result = ranksums(
        coupling_data[0],
        coupling_data[1],
    )
    print(
        f" pvalue between wavelet power {combination[0]} {combination[1]}: ",
        wilcoxon_result.pvalue,
    )
    return float(wilcoxon_result.pvalue)


def calculate_coupling_strength(
    file_path: Path,
) -> Tuple[float, float, Tuple[int, int]]:
    combination = get_channel_combination(str(file_path))
    cc = load_data(file_path)
    cpl_1, cpl_2, _ = dirc(cc)
    return cpl_1, cpl_2, combination


# TODO: THIS IS NOT GOOD WE NEED THIS FOR EVERY PATIENT D:
def mean_freq_map_main() -> None:
    check_folder(P_VALUES_STATE_FOLDER)
    combination_dict_list: Dict[Tuple[int, int], List[Tuple[float, float]]] = {
        key: [] for key in ALL_COMBINATIONS
    }

    experiment_combination = {
        "a": copy.deepcopy(combination_dict_list),
        "b": copy.deepcopy(combination_dict_list),
    }

    p_value_json_data: Dict[str, float] = {}

    data_files = [file for file in BAYESIAN_DATA_PATH.rglob("*.pickle")]

    a_experiment = [
        file_path for file_path in data_files if "a" in str(file_path).split("/")[-1]
    ]
    b_experiment = [
        file_path for file_path in data_files if "b" in str(file_path).split("/")[-1]
    ]

    assert len(a_experiment) + len(b_experiment) == len(data_files)

    for file_path in a_experiment:
        cpl_1, cpl_2, combination = calculate_coupling_strength(file_path)
        if cpl_1 < 10000 or cpl_2 < 10000:
            experiment_combination["a"][combination].append((cpl_1, cpl_2))

    for file_path in b_experiment:
        cpl_1, cpl_2, combination = calculate_coupling_strength(file_path)
        if cpl_1 < 10000 or cpl_2 < 10000:
            experiment_combination["b"][combination].append((cpl_1, cpl_2))

    for combination in combination_dict_list:
        a_strength = np.array(experiment_combination["a"][combination])
        b_strength = np.array(experiment_combination["b"][combination])
        for i in range(2):
            a_state_coupling = a_strength[:, i]
            b_state_coupling = b_strength[:, i]
            p_value = p_test([a_state_coupling, b_state_coupling], combination)
            p_value_json_data[f"{combination}{i}"] = p_value

    # REVIEW: Rewrite this with pathlib
    with open(
        str(P_VALUES_STATE_FOLDER / "pvalues_state_experiment.json"), "w"
    ) as outfile:
        json.dump(p_value_json_data, outfile)


if __name__ == "__main__":
    mean_freq_map_main()
