import argparse
import pickle
from pathlib import Path
from typing import Dict, Tuple

import numpy as np
from joblib import Parallel, delayed
from processing.phase_functions import co_fbtransf1, co_hilbproto

# NOTE: Uncomment this when numba supports numpy >= 1.21
# numpy 1.21 has its own typing stubs that are deemed more helpfull when typing code
# from processing.phase_functions_jit import co_fbtransf1
from processing.utils.constants import (
    ALL_COMBINATIONS,
    PHASE_OUTPUT_PATH,
    UNPACKED_SIGNALS_PATH,
)
from processing.utils.data_utils import remove_and_create_folders, save_file
from tqdm import tqdm


def _constuct_save_name(
    file_: Path, first_combination: int, second_combination: int
) -> Path:
    end_string = f"({first_combination},{second_combination})"
    file_name = file_.stem + end_string
    return Path(PHASE_OUTPUT_PATH / file_name).with_suffix(".pickle")


def construct_phases(
    eeg_signals: np.ndarray, combination: Tuple[int, int]
) -> Dict[str, np.ndarray]:
    data = {}
    first_channel_index = combination[0] - 1
    second_channel_index = combination[1] - 1

    assert 0 <= first_channel_index <= 7
    assert 0 <= second_channel_index <= 7

    first_channel = eeg_signals[first_channel_index, :]
    second_channel = eeg_signals[second_channel_index, :]

    theta_first = co_hilbproto(first_channel, ntail=0)
    theta_second = co_hilbproto(second_channel, ntail=0)

    phi_first, _, _ = co_fbtransf1(theta_first, nfft=80, alpha=0.05, ngrid=50)
    phi_second, _, _ = co_fbtransf1(theta_second, nfft=80, alpha=0.05, ngrid=50)

    data[f"channel_{first_channel_index}"] = phi_first
    data[f"channel_{second_channel_index}"] = phi_second
    return data


def export_phases_main(file_path: Path) -> None:
    patient = pickle.loads(file_path.read_bytes())
    eeg_signals = patient["eeg"]
    for combination in ALL_COMBINATIONS:
        first_channel, second_channel = combination
        location = _constuct_save_name(file_, first_channel, second_channel)
        assert eeg_signals.shape[0] == 8
        data = construct_phases(eeg_signals, combination)
        save_file(data, location)


def parallel_args(parallelisation: str) -> bool:
    if parallelisation == "not_parallel":
        return False

    if parallelisation == "parallel":
        return True

    raise (Exception("The arguments specified must be either: parallel | not_parallel"))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "parallelisation",
        nargs="?",
        help="parallelisation of the process parallel, not_parallel",
        default="not_parallel",
    )

    args = parser.parse_args()
    PARALLEL = parallel_args(args.parallelisation)
    FILE_PATHS = [path for path in UNPACKED_SIGNALS_PATH.rglob("*.pickle")]
    remove_and_create_folders(PHASE_OUTPUT_PATH)
    if not PARALLEL:
        for file_ in tqdm(FILE_PATHS):
            export_phases_main(file_)

    if PARALLEL:
        Parallel(n_jobs=4)(
            delayed(export_phases_main)(file_) for file_ in tqdm(FILE_PATHS)
        )
