from pathlib import Path

from processing.bayesian_inference import BayesianInference
from processing.utils.constants import (
    OVERLAP,
    PHASE_OUTPUT_PATH,
    RE_SAMPLE_FREQ,
    WINDOW_SIZE,
)
from processing.utils.data_utils import load_phases, setup_logging
from tqdm import tqdm

setup_logging()


def construct_name(file_path: Path) -> Path:
    return Path(file_path.with_suffix("").stem)


def patient_bayes_main() -> None:
    file_paths = [path for path in PHASE_OUTPUT_PATH.rglob("*.pickle")]

    bayesian = BayesianInference(WINDOW_SIZE, 1 / RE_SAMPLE_FREQ, OVERLAP)
    bayesian.check_folder()

    for file_path in tqdm(file_paths):
        file_name = construct_name(file_path)
        first_phase, second_phase = load_phases(file_path)
        cc, _ = bayesian(first_phase, second_phase)
        bayesian.save_inferred_data(file_name)


if __name__ == "__main__":
    patient_bayes_main()
