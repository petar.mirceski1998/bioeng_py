from typing import Dict, Optional, Tuple

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.axes_grid1 import make_axes_locatable

from processing.mean_parameter_calculation import (
    construct_mean_maps,
    mean_parameter_calculation,
)
from processing.utils.constants import BAYESIAN_DATA_PATH, CHANNELS, FREQUENCY_MAP_PATH
from processing.utils.data_utils import check_folder

ticks = np.arange(0.5, 8.5)
CHANNELS = [channel.replace("EEG", "").strip() for channel in CHANNELS]


# TODO: REFORMAT THIS SO AX IS A TUPLE OF ELEMENTS AND
#       PROGRAMATIVALY CALL IT SO IT TAKES A LOT LESS SPACE
def set_ticks(ax: matplotlib.axes) -> None:
    ax.set_xticks(ticks)
    ax.set_xticklabels(CHANNELS, rotation="vertical", fontsize=6)
    ax.set_yticks(ticks)
    ax.set_yticklabels(CHANNELS, rotation="horizontal", fontsize=6)


def color_map(
    frequency_mean_dict: Dict[Tuple[int, int], np.ndarray], save_name: Optional[str]
) -> None:

    color_map_2_1, color_map_1_2, _ = construct_mean_maps(frequency_mean_dict)

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 8))

    c = ax1.pcolor(color_map_2_1, cmap="hot")
    ax1.set_aspect("equal", "box")
    divider = make_axes_locatable(ax1)
    cax1 = divider.append_axes("right", size="5%", pad=0.05)
    fig.colorbar(c, cax=cax1)
    ax1.set_title(r"Coupling strength $q_2$ to $q_1$")
    ax1.set_ylabel(r"$q_1$")
    ax1.set_xlabel(r"$q_2$")
    set_ticks(ax1)

    c = ax2.pcolor(color_map_1_2, cmap="hot")
    ax2.set_aspect("equal", "box")
    divider = make_axes_locatable(ax2)
    cax2 = divider.append_axes("right", size="5%", pad=0.05)
    fig.colorbar(c, cax=cax2)
    ax2.set_title(r"Coupling strength $q_1$ to $q_2$")
    ax2.set_ylabel(r"$q_2$")
    ax2.set_xlabel(r"$q_1$")
    set_ticks(ax2)

    # c = ax3.pcolor(direction_map, cmap="hot")
    # ax3.set_aspect("equal", "box")
    # divider = make_axes_locatable(ax3)
    # cax3 = divider.append_axes("right", size="5%", pad=0.05)
    # fig.colorbar(c, cax=cax3)
    # ax3.set_title(r"Direction of coupling")
    # set_ticks(ax3)

    fig.tight_layout()
    # mng = plt.get_current_fig_manager()
    # mng = plt.get_current_fig_manager()
    # mng.window.showMaximized()

    if save_name is not None:
        plt.savefig(save_name, dpi=100, bbox_inches="tight", pad_inches=0)
        plt.close()
    else:
        plt.show()


def mean_freq_map_main() -> None:
    check_folder(FREQUENCY_MAP_PATH)
    data_files = [file for file in BAYESIAN_DATA_PATH.rglob("*.pickle")]

    five_hz = [file_path for file_path in data_files if "5Hz" in str(file_path)]
    six_hz = [file_path for file_path in data_files if "6Hz" in str(file_path)]
    ten_hz = [file_path for file_path in data_files if "10Hz" in str(file_path)]

    assert len(five_hz) + len(ten_hz) + len(six_hz) == len(data_files)

    five_hz_mean = mean_parameter_calculation(five_hz)
    six_hz_mean = mean_parameter_calculation(six_hz)
    ten_hz_mean = mean_parameter_calculation(ten_hz)

    color_map(five_hz_mean, str(FREQUENCY_MAP_PATH / "five_hz_heatmap.png"))
    color_map(six_hz_mean, str(FREQUENCY_MAP_PATH / "six_hz_heatmap.png"))
    color_map(ten_hz_mean, str(FREQUENCY_MAP_PATH / "ten_hz_heatmap.png"))


if __name__ == "__main__":
    mean_freq_map_main()
