import os
import pickle
import shutil
from pathlib import Path
from typing import Dict

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pywt
from CWT import cws
from tqdm import tqdm


def test_scales() -> None:
    frequencies = pywt.scale2frequency("cmor1-10", scales) / dt
    print(frequencies)
    plt.scatter(np.arange(frequencies.shape[0]), frequencies)
    plt.show()


PLOT = False
TIME_DIR = Path("./unpacked_signals/")
WAVELET_DATA_FOLDER = Path("./wavelet_data")
dt = 1 / 100

scales = np.arange(29, 0.8, -0.05)
scales = 10 / (scales * dt)
frequencies = pywt.scale2frequency("cmor1-10", scales) / dt

FILE_PATHS = TIME_DIR / pd.read_csv("RECORDS", sep=" ", header=None).values.ravel()
FILE_PATHS = [file.with_suffix(".pickle") for file in FILE_PATHS]


def construct_directories() -> None:
    if os.path.isdir(WAVELET_DATA_FOLDER):
        shutil.rmtree(WAVELET_DATA_FOLDER)
    os.mkdir(WAVELET_DATA_FOLDER)


# REVIEW: This is bad rewrite typing and way of reading files with pathlib
def open_file(path: str) -> Dict[str, np.ndarray]:
    infile = open(path, "rb")
    patient = pickle.load(infile, encoding="utf-8")
    infile.close()
    return patient["eeg"]


def process_signal(eeg_signal: np.ndarray, path: str, index: int) -> None:
    time_scales = np.arange(eeg_signal.shape[0]) * dt
    wavelet_obj = cws.CWT(time_scales, eeg_signal, scales=scales, wavelet="cmorl1-10")
    coefficients = wavelet_obj.coefs
    frequencies = wavelet_obj.scales_freq
    data = {"time": time_scales, "coefs": coefficients, "frequencies": frequencies}
    location = WAVELET_DATA_FOLDER / str(path).split(os.path.sep)[-1]
    with open(str(location), "wb") as handle:
        pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)


def process_data(path: str) -> None:
    eeg_data = open_file(path)
    for index, eeg_signal in enumerate(eeg_data):
        process_signal(eeg_signal, path, index)


def main() -> None:
    construct_directories()
    for file_ in tqdm(FILE_PATHS):
        process_data(file_)


if __name__ == "__main__":
    main()
