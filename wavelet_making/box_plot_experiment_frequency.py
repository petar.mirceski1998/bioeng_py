import os
import pickle
import shutil
from pathlib import Path
from typing import Dict, List, Optional, Tuple

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

DATA_PATH = Path("./power_bands/")
SAVE_FOLDER = Path("./box_plots/box_experiment_frequency")
FILE_PATHS = [file_path for file_path in DATA_PATH.rglob("*.pickle")]


def construct_directories() -> None:
    if os.path.isdir(SAVE_FOLDER):
        shutil.rmtree(SAVE_FOLDER)
    os.mkdir(SAVE_FOLDER)


def bar_plot_data(data: np.ndarray, experiment_hz: Optional[str]) -> None:
    plt.figure(figsize=(20, 10))
    plt.boxplot(data)
    plt.title(
        f"Box plot of power frequency bands power spectrum experiment {experiment_hz}"
    )
    plt.xticks([1, 2], [r"$\theta$", r"$\alpha$"])
    plt.xlabel("Neural frequency band")
    if experiment_hz is not None:
        plt.savefig(str(SAVE_FOLDER / experiment_hz))
    else:
        plt.show()


def open_file(path: Path) -> Tuple[float, float, str]:
    infile = open(path, "rb")
    patient = pickle.load(infile, encoding="utf-8")
    infile.close()
    five_hz = patient["five_hz"]
    ten_hz = patient["ten_hz"]
    hz = path.name.split("_")[1]
    return five_hz, ten_hz, hz


def main() -> None:
    experiment: Dict[str, List[Tuple[float, float]]] = {
        "5Hz": [],
        "6Hz": [],
        "10Hz": [],
    }
    construct_directories()
    for file_ in tqdm(FILE_PATHS):
        five, ten, hz = open_file(file_)
        experiment[hz].append((five, ten))

    for hz, power in experiment.items():
        bar_plot_data(np.array(power), hz)


if __name__ == "__main__":
    main()
