import os
import pickle
import shutil
from typing import Dict

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pywt
from tqdm import tqdm

from CWT import cws


def test_scales() -> None:
    frequencies = pywt.scale2frequency("cmor1-10", scales) / dt
    print(frequencies)
    plt.scatter(np.arange(frequencies.shape[0]), frequencies)
    plt.show()


PLOT = False
TIME_DIR = "./unpacked_signals/"
UNPACK_FOLDERS = ["5-Hz", "6-Hz", "10-Hz"]
WAVELET_IMAGES_FOLDER = "./wavelet_images_cmorl1-10/"
dt = 1 / 100

scales = np.arange(29, 0.8, -0.2)
scales = 10 / (scales * dt)

FILE_PATHS = TIME_DIR + pd.read_csv("RECORDS", sep=" ", header=None).values.ravel()
FILE_PATHS = [file.replace(".edf", ".pickle") for file in FILE_PATHS]
# test_scales()


def construct_directories() -> None:
    if os.path.isdir(WAVELET_IMAGES_FOLDER):
        shutil.rmtree(WAVELET_IMAGES_FOLDER)
    os.mkdir(WAVELET_IMAGES_FOLDER)

    for folder in UNPACK_FOLDERS:
        os.mkdir(WAVELET_IMAGES_FOLDER + folder)


def save_wavelet_map(wavelet_obj: cws.CWT, path: str) -> None:
    # Create Fig and gridspec
    fig = plt.figure(figsize=(20, 20), dpi=80)
    grid = plt.GridSpec(4, 4, hspace=0.5, wspace=0.2)

    # Define the axes
    ax_main = fig.add_subplot(grid[:-1, :-1])
    ax_right = fig.add_subplot(
        grid[:-1, -1], xticklabels=[], yticklabels=[], sharey=ax_main
    )
    ax_bottom = fig.add_subplot(
        grid[-1, 0:-1], xticklabels=[], yticklabels=[], sharex=ax_main
    )
    plt.cla()

    # Main axis wavelet Plot
    ax_main, ylim = cws.cws(
        wavelet_obj, yaxis="frequency", yscale="linear", ax=ax_main, spectrum="power",
    )
    ax_main.title.set_fontsize(20)
    plt.cla()

    # Bottom signal plot
    ax_bottom.set_title("signal")
    ax_bottom.title.set_fontsize(20)
    ax_bottom.plot(wavelet_obj.time, wavelet_obj.signal, color="red", lw=1)

    # Right Dignal Density Plot of Wavelet
    ax_right.set_title("frequency density \n time mean")
    ax_right.title.set_fontsize(20)
    ax_right.plot(
        (np.abs(wavelet_obj.coefs) ** 2).mean(axis=1),
        wavelet_obj.scales_freq,
        color="deeppink",
    )
    # Decorations
    for item in (
        [ax_main.xaxis.label, ax_main.yaxis.label]
        + ax_main.get_xticklabels()
        + ax_main.get_yticklabels()
    ):
        item.set_fontsize(14)

    ax_right.set_yscale("linear")
    xlabels = ax_main.get_xticks().tolist()
    ax_main.set_xticklabels(xlabels)
    xrlabels = ax_right.get_xticks().tolist()
    ax_right.set_xticklabels(xrlabels)
    ax_right.xaxis.set_major_locator(plt.MaxNLocator(5))

    # THS IS THE Y TICKS HAND CRAFTED
    y_range = ax_main.get_yticks()
    ax_main.yaxis.set_ticks(np.arange(y_range[0], y_range[-1] + 1, 5))
    ax_right.set_ylim(*ylim)
    ax_right.xaxis.set_major_locator(plt.MaxNLocator(5))

    if not PLOT:
        plt.savefig(fname=path)
        plt.cla()
        plt.close()
    else:
        plt.show()


def open_file(path: str) -> Dict[str, np.ndarray]:
    infile = open(path, "rb")
    patient = pickle.load(infile, encoding="utf-8")
    infile.close()
    return patient["eeg"]


def process_signal(eeg_signal: np.ndarray, path: str, index: int) -> None:
    time_scales = np.arange(eeg_signal.shape[0]) * dt
    wavelet_obj = cws.CWT(time_scales, eeg_signal, scales=scales, wavelet="cmorl1-10")
    save_path = path.replace(".pickle", f"{index}.png")
    save_path = save_path.replace("unpacked_signals/", WAVELET_IMAGES_FOLDER)
    save_wavelet_map(wavelet_obj, save_path)


def process_data(path: str) -> None:
    eeg_data = open_file(path)
    for index, eeg_signal in enumerate(eeg_data):
        process_signal(eeg_signal, path, index)


def main() -> None:
    construct_directories()
    for file_ in tqdm(FILE_PATHS):
        process_data(file_)


if __name__ == "__main__":
    main()
