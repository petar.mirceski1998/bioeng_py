import os
import pickle
import shutil
from pathlib import Path
from typing import Dict, List, Tuple

import numpy as np
import pandas as pd
from tqdm import tqdm

DATA_PATH = Path("./power_bands/")
SAVE_FOLDER = Path("./box_plots/")
FILE_PATHS = [file_path for file_path in DATA_PATH.rglob("*.pickle")]


def construct_directories() -> None:
    if os.path.isdir(SAVE_FOLDER):
        shutil.rmtree(SAVE_FOLDER)
    os.mkdir(SAVE_FOLDER)


def export_data(data: np.ndarray, experiment_hz: str = "") -> None:
    power_data_frame = pd.DataFrame(data, columns=["5Hz", "6Hz", "10Hz"])
    power_data_frame.to_csv(f"{experiment_hz}_all_bands.csv", index=False)


def open_file(path: Path) -> Tuple[float, float, float, str]:
    infile = open(path, "rb")
    patient = pickle.load(infile, encoding="utf-8")
    infile.close()
    five_hz = patient["five_hz"]
    six_hz = patient["six_hz"]
    ten_hz = patient["ten_hz"]
    hz = path.name.split("_")[1]
    return five_hz, six_hz, ten_hz, hz


def main() -> None:
    experiment: Dict[str, List[Tuple[float, float, float]]] = {
        "5Hz": [],
        "6Hz": [],
        "10Hz": [],
    }
    construct_directories()
    for file_ in tqdm(FILE_PATHS):
        five, six, ten, hz = open_file(file_)
        experiment[hz].append((five, six, ten))

    for hz, power in experiment.items():
        export_data(np.array(power), hz)


if __name__ == "__main__":
    main()
