import os
import pickle
import shutil
from pathlib import Path
from typing import Dict, Tuple

import numpy as np
from joblib import Parallel, delayed
from tqdm import tqdm

DATA_PATH = Path("./wavelet_data/")
SAVE_FOLDER = Path("./power_bands_two/")
FILE_PATHS = [file_path for file_path in DATA_PATH.rglob("*.pickle")]
PARALLEL = True


def construct_directories() -> None:
    if os.path.isdir(SAVE_FOLDER):
        shutil.rmtree(SAVE_FOLDER)
    os.mkdir(SAVE_FOLDER)


def open_file(path: str) -> Tuple[np.ndarray, np.ndarray, np.ndarray, str]:
    infile = open(path, "rb")
    patient = pickle.load(infile, encoding="utf-8")
    infile.close()
    time = patient["time"]
    coefs = patient["coefs"]
    frequencies = patient["frequencies"]
    experiment_freq = str(path).split("_")[2]
    return coefs, time, frequencies, experiment_freq


def filter_and_integrate_signal(
    coefs: np.ndarray, five_hz: np.ndarray, ten_hz: np.ndarray
) -> Dict[str, float]:

    five_hz_band = np.abs(coefs[five_hz])
    ten_hz_band = np.abs(coefs[ten_hz])

    five_hz_integral = np.mean(np.power(five_hz_band, 2))
    ten_hz_integral = np.mean(np.power(ten_hz_band, 2))

    return {
        "five_hz": five_hz_integral,
        "ten_hz": ten_hz_integral,
    }


def process_data(path: str, five_hz: np.ndarray, ten_hz: np.ndarray) -> None:
    coefs, _, frequencies, experiment_freq = open_file(path)
    data = filter_and_integrate_signal(coefs, five_hz, ten_hz)
    location = SAVE_FOLDER / str(path).split(os.path.sep)[-1]
    with open(str(location), "wb") as handle:
        pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)


def get_freq_range(path: Path) -> np.ndarray:
    coefs, _, frequencies, experiment_freq = open_file(str(path))
    return frequencies


def main() -> None:
    construct_directories()
    frequencies = get_freq_range(FILE_PATHS[0])
    five_hz = np.where((4 <= frequencies) & (frequencies < 7.5))
    ten_hz = np.where((7.5 < frequencies) & (frequencies <= 14.5))

    if not PARALLEL:
        for file_ in tqdm(FILE_PATHS):
            process_data(file_, five_hz, ten_hz)

    if PARALLEL:
        Parallel(n_jobs=4)(
            delayed(process_data)(file_, five_hz, ten_hz) for file_ in tqdm(FILE_PATHS)
        )


if __name__ == "__main__":
    main()
