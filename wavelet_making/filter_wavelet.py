import os
import pickle
import shutil
from pathlib import Path
from typing import Dict, Tuple

import numpy as np
from tqdm import tqdm

EPSILON = 0.1
dt = 1 / 100

DATA_PATH = Path("./wavelet_data/")
SAVE_FOLDER = Path("./power_bands/")
FILE_PATHS = [file_path for file_path in DATA_PATH.rglob("*.pickle")]


def construct_directories() -> None:
    if os.path.isdir(SAVE_FOLDER):
        shutil.rmtree(SAVE_FOLDER)
    os.mkdir(SAVE_FOLDER)


def open_file(path: str) -> Tuple[np.ndarray, np.ndarray, np.ndarray, str]:
    infile = open(path, "rb")
    patient = pickle.load(infile, encoding="utf-8")
    infile.close()
    time = patient["time"]
    coefs = patient["coefs"]
    frequencies = patient["frequencies"]
    experiment_freq = str(path).split("_")[2]
    return coefs, time, frequencies, experiment_freq


def filter_and_integrate_signal(
    coefs: np.ndarray, frequencies: np.ndarray
) -> Dict[str, float]:

    five_hz = np.where((4.5 <= frequencies) & (frequencies <= 5.5))
    six_hz = np.where((5.5 < frequencies) & (frequencies <= 6.5))
    ten_hz = np.where((9.5 <= frequencies) & (frequencies <= 11.5))

    five_hz_band = np.abs(coefs[five_hz])
    six_hz_band = np.abs(coefs[six_hz])
    ten_hz_band = np.abs(coefs[ten_hz])

    five_hz_integral = np.mean(np.power(five_hz_band, 2))
    six_hz_integral = np.mean(np.power(six_hz_band, 2))
    ten_hz_integral = np.mean(np.power(ten_hz_band, 2))

    return {
        "five_hz": five_hz_integral,
        "six_hz": six_hz_integral,
        "ten_hz": ten_hz_integral,
    }


def process_data(path: str) -> None:
    coefs, _, frequencies, _ = open_file(path)
    data = filter_and_integrate_signal(coefs, frequencies)
    location = SAVE_FOLDER / str(path).split(os.path.sep)[-1]
    with open(str(location), "wb") as handle:
        pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)


def main() -> None:
    construct_directories()
    for file_ in tqdm(FILE_PATHS):
        process_data(file_)


if __name__ == "__main__":
    main()
