import json
import os
import pickle
import shutil
from pathlib import Path
from typing import Dict, List, Tuple

import numpy as np
from scipy.stats import ranksums
from tqdm import tqdm

DATA_PATH = Path("./power_bands_two/")
FILE_PATHS = [file_path for file_path in DATA_PATH.rglob("*.pickle")]
SAVE_FOLDER = Path("./p_values/experiment_freq_two")

experiment_codes: Dict[int, str] = {0: "5Hz", 1: "10Hz"}


def construct_directories() -> None:
    if os.path.isdir(SAVE_FOLDER):
        shutil.rmtree(SAVE_FOLDER)
    os.mkdir(SAVE_FOLDER)


def p_test(data: np.ndarray) -> Dict[str, float]:
    p_values_for_freq: Dict[str, float] = {}
    wilcoxon_result = ranksums(data[:, 0], data[:, 1])
    print(
        f" pvalue between wavelet power {experiment_codes[0]} {experiment_codes[1]}: ",
        wilcoxon_result.pvalue,
    )
    tuple_combination = (
        experiment_codes[0],
        experiment_codes[1],
    )
    key_combination = "".join(tuple_combination)
    p_values_for_freq[key_combination] = float(wilcoxon_result.pvalue)

    return p_values_for_freq


def open_file(path: Path) -> Tuple[float, float, str]:
    infile = open(path, "rb")
    patient = pickle.load(infile, encoding="utf-8")
    infile.close()
    five_hz = patient["five_hz"]
    ten_hz = patient["ten_hz"]
    hz = path.name.split("_")[1]
    return five_hz, ten_hz, hz


def main() -> None:
    p_value_json_data: Dict[str, Dict[str, float]] = {}
    construct_directories()
    experiment: Dict[str, List[Tuple[float, float]]] = {
        "5Hz": [],
        "6Hz": [],
        "10Hz": [],
    }
    for file_ in tqdm(FILE_PATHS):
        five, ten, hz = open_file(file_)
        experiment[hz].append((five, ten))

    for hz, power in experiment.items():
        print(f"P-Value across experiments for band frequency: {hz}")
        p_value_dict = p_test(np.array(power))
        p_value_json_data[hz] = p_value_dict

    with open(str(SAVE_FOLDER / "pvalues_experiment_frequency.json"), "w") as outfile:
        json.dump(p_value_json_data, outfile)


if __name__ == "__main__":
    main()
