import json
import os
import pickle
import shutil
from itertools import combinations
from pathlib import Path
from typing import Dict, List, Tuple

import numpy as np
from scipy.stats import ranksums
from tqdm import tqdm

DATA_PATH = Path("./power_bands/")
FILE_PATHS = [file_path for file_path in DATA_PATH.rglob("*.pickle")]
SAVE_FOLDER = Path("./p_values/freq_experiment")

experiment_codes: Dict[int, str] = {0: "5Hz", 1: "6Hz", 2: "10Hz"}
experiment_combinations: List[Tuple[int, int]] = [
    (comb[0], comb[1]) for comb in combinations(range(3), 2)
]


def construct_directories() -> None:
    if os.path.isdir(SAVE_FOLDER):
        shutil.rmtree(SAVE_FOLDER)
    os.mkdir(SAVE_FOLDER)


def p_test(same_band_data: List[np.ndarray]) -> Dict[str, float]:
    p_values_for_freq: Dict[str, float] = {}

    for first_experiment_index, second_experiment_index in experiment_combinations:
        wilcoxon_result = ranksums(
            same_band_data[first_experiment_index],
            same_band_data[second_experiment_index],
        )
        print(
            f" pvalue between wavelet power {experiment_codes[first_experiment_index]} {experiment_codes[second_experiment_index]}: ",
            wilcoxon_result.pvalue,
        )
        tuple_combination = (
            experiment_codes[first_experiment_index],
            experiment_codes[second_experiment_index],
        )
        key_combination = "".join(tuple_combination)
        p_values_for_freq[key_combination] = float(wilcoxon_result.pvalue)

    return p_values_for_freq


def open_file(path: Path) -> Tuple[float, float, float, str]:
    infile = open(path, "rb")
    patient = pickle.load(infile, encoding="utf-8")
    infile.close()
    five_hz = patient["five_hz"]
    six_hz = patient["six_hz"]
    ten_hz = patient["ten_hz"]
    hz = path.name.split("_")[1]
    return five_hz, six_hz, ten_hz, hz


def main() -> None:
    p_value_json_data: Dict[str, Dict[str, float]] = {}
    construct_directories()
    experiment: Dict[str, List[Tuple[float, float, float]]] = {
        "5Hz": [],
        "6Hz": [],
        "10Hz": [],
    }
    for file_ in tqdm(FILE_PATHS):
        five, six, ten, hz = open_file(file_)
        experiment[hz].append((five, six, ten))

    for band_index in range(3):
        print(
            f"P-Value across experiments for band frequency: {experiment_codes[band_index]}"
        )
        five_hz_same_band = np.array(experiment["5Hz"])[:, band_index]
        six_hz_same_band = np.array(experiment["6Hz"])[:, band_index]
        ten_hz_same_band = np.array(experiment["10Hz"])[:, band_index]
        p_value_dict = p_test([five_hz_same_band, six_hz_same_band, ten_hz_same_band])
        p_value_json_data[experiment_codes[band_index]] = p_value_dict

    with open(str(SAVE_FOLDER / "pvalues_same_freq_experiment.json"), "w") as outfile:
        json.dump(p_value_json_data, outfile)


if __name__ == "__main__":
    main()
