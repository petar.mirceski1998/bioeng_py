import json
import os
import pickle
import shutil
from itertools import combinations
from pathlib import Path
from typing import Dict, List, Tuple

import numpy as np
from scipy.stats import ranksums
from tqdm import tqdm

DATA_PATH = Path("./power_bands/")
FILE_PATHS = [file_path for file_path in DATA_PATH.rglob("*.pickle")]
SAVE_FOLDER = Path("./p_values/experiment_freq")

experiment_codes: Dict[int, str] = {0: "5Hz", 1: "6Hz", 2: "10Hz"}
experiment_combinations: List[Tuple[int, int]] = [
    (comb[0], comb[1]) for comb in combinations(range(3), 2)
]


def construct_directories() -> None:
    if os.path.isdir(SAVE_FOLDER):
        shutil.rmtree(SAVE_FOLDER)
    os.mkdir(SAVE_FOLDER)


def p_test(data: np.ndarray) -> Dict[str, float]:
    p_values_for_freq: Dict[str, float] = {}
    for first_experiment_index, second_experiment_index in experiment_combinations:
        wilcoxon_result = ranksums(
            data[:, first_experiment_index], data[:, second_experiment_index]
        )
        print(
            f" pvalue between wavelet power {experiment_codes[first_experiment_index]} {experiment_codes[second_experiment_index]}: ",
            wilcoxon_result.pvalue,
        )
        tuple_combination = (
            experiment_codes[first_experiment_index],
            experiment_codes[second_experiment_index],
        )
        key_combination = "".join(tuple_combination)
        p_values_for_freq[key_combination] = float(wilcoxon_result.pvalue)

    return p_values_for_freq


def open_file(path: Path) -> Tuple[float, float, float, str]:
    infile = open(path, "rb")
    patient = pickle.load(infile, encoding="utf-8")
    infile.close()
    five_hz = patient["five_hz"]
    six_hz = patient["six_hz"]
    ten_hz = patient["ten_hz"]
    hz = path.name.split("_")[1]
    return five_hz, six_hz, ten_hz, hz


def main() -> None:
    p_value_json_data: Dict[str, Dict[str, float]] = {}
    construct_directories()
    experiment: Dict[str, List[Tuple[float, float, float]]] = {
        "5Hz": [],
        "6Hz": [],
        "10Hz": [],
    }
    for file_ in tqdm(FILE_PATHS):
        five, six, ten, hz = open_file(file_)
        experiment[hz].append((five, six, ten))

    for hz, power in experiment.items():
        print(f"P-Value across experiments for band frequency: {hz}")
        p_value_dict = p_test(np.array(power))
        p_value_json_data[hz] = p_value_dict

    with open(str(SAVE_FOLDER / "pvalues_experiment_frequency.json"), "w") as outfile:
        json.dump(p_value_json_data, outfile)


if __name__ == "__main__":
    main()
