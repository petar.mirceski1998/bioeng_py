import os
import shutil

import matplotlib.pyplot as plt
import numpy as np
from scipy.fft import fft, fftfreq, fftshift

SAVE_FIG: bool = True
SAVE_FOLDER: str = "./wavelet_plots"


def folder_exists() -> None:
    if os.path.isdir(SAVE_FOLDER):
        shutil.rmtree(SAVE_FOLDER)
    os.mkdir(SAVE_FOLDER)


def morlet(t: float, w0: float) -> np.complex:
    morl = np.pi ** (-0.25) * np.exp(-1j * w0 * t) * np.exp(-(t ** 2) * 0.5)
    return morl


def time_domain_morlet(w0: float, save_fig: bool = True) -> None:
    t_series, dt = np.linspace(-5, 5, 100, retstep=True)
    morlet_sim = morlet(t_series, w0=w0)

    title = f"Morlet wavelet in the time domain $\\omega_0={w0}$"
    plt.figure()
    plt.plot(t_series, np.real(morlet_sim))
    plt.title(r"{}".format(title))
    plt.xlabel(r"$Time(s)$")
    plt.ylabel(r"$\psi(t)$")
    if not save_fig:
        plt.show()
    elif save_fig:
        plt.savefig(f"{SAVE_FOLDER}/time_domain_wavelet_w0{w0}.png")


def frequency_domain_morlet(w0: float, save_fig: bool = True) -> None:
    t_series, dt = np.linspace(-5, 5, 100, retstep=True)
    morlet_sim = morlet(t_series, w0=w0)

    title = f"Morlet wavelet in the frequency domain $\\omega_0={w0}$"
    morlet_sim = morlet_sim.real
    f_morl = fftshift(fft(morlet_sim))
    freq = fftshift(fftfreq(len(t_series), dt))

    plt.figure()
    plt.plot(freq * 2 * np.pi, np.abs(f_morl))
    plt.xlim([w0 - 7, w0 + 7])
    plt.title(r"{}".format(title))
    plt.xlabel(r"$Frequency(f)$")
    plt.ylabel(r"$\psi(t)$")
    if not save_fig:
        plt.show()
    elif save_fig:
        plt.savefig(f"{SAVE_FOLDER}/frequency_domain_wavelet_w0{w0}.png")


def overlap_time_domain(w01: float, w02: float, save_fig: bool = True) -> None:
    t_series, dt = np.linspace(-5, 5, 100, retstep=True)
    morlet_sim1 = morlet(t_series, w0=w01)
    morlet_sim2 = morlet(t_series, w0=w02)

    omega1 = r"{}".format(f"$\\omega_{{0 1}} = {w01}$")
    omega2 = r"{}".format(f"$\\omega_{{0 2}} = {w02}$")
    title = f"Time domain overlap {omega1} {omega2}"
    x_title = r"$Time(s)$"
    y_title = r"$\psi(t)$"

    plt.figure()
    plot1 = plt.plot(t_series, np.real(morlet_sim1))
    plot2 = plt.plot(t_series, np.real(morlet_sim2))
    plt.title(r"{}".format(title))
    plt.xlabel(x_title)
    plt.ylabel(y_title)
    plt.legend((plot1[0], plot2[0]), (omega1, omega2))
    if not save_fig:
        plt.show()
    elif save_fig:
        plt.savefig(f"{SAVE_FOLDER}/overlap_time_domain_wavelet_w01{w01}_w01{w02}.png")


def main() -> None:
    folder_exists()
    w0_space = [1, 5, 10]
    overlap_time_domain(5, 10, save_fig=SAVE_FIG)
    for w0 in w0_space:
        time_domain_morlet(w0=w0, save_fig=SAVE_FIG)
        frequency_domain_morlet(w0=w0, save_fig=SAVE_FIG)


if __name__ == "__main__":
    main()
